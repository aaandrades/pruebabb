const { Router } = require('express');
const router = Router();

const { getPerson, createPerson, queryPerson, deletePerson, updatePerson } = require('../controllers/index.controller.js');


//Route getPerson
router.get('/getPerson', getPerson);
//Route createPerson
router.post('/createPerson', createPerson);
//Route query Id for Person
router.get('/consultPerson/:id', queryPerson);
//Route Delete Id for Person
router.delete('/deletePerson/:id', deletePerson);
//Route Update Person
router.put('/updatePerson/:id', updatePerson);


module.exports = router;