"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CrearPersonaComponent = void 0;
var core_1 = require("@angular/core");
var CrearPersonaComponent = /** @class */ (function () {
    function CrearPersonaComponent() {
        this.id = '';
        this.nombre = '';
        this.fecha = '';
        this.ROOT_URL = 'http://localhost:3000/';
    }
    //Obtener Valores del form
    CrearPersonaComponent.prototype.getTextBoxVal = function (id, nombre, fecha, email) {
        console.log(id.value);
        console.log(nombre.value);
        console.log(fecha.value);
        console.log(email.value);
    };
    CrearPersonaComponent.prototype.ngOnInit = function () {
    };
    CrearPersonaComponent = __decorate([
        core_1.Component({
            selector: 'app-crear-persona',
            templateUrl: './crear-persona.component.html',
            styleUrls: ['./crear-persona.component.css']
        })
    ], CrearPersonaComponent);
    return CrearPersonaComponent;
}());
exports.CrearPersonaComponent = CrearPersonaComponent;
