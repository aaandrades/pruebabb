import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  apiUrl = 'http://localhost:3000/getPerson';

  constructor(private _http: HttpClientModule) {
    
    getPerson(){
      return this._http.get<User[]>(this.apiUrl)
    }
   }
}
