import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'personapp';

  //Activar capas con ngIf y validacion
  crear= false;
  ver = false;
  modificar = false;
  eliminar = false;
  adoptar = false;


}
