# PRESENTACIÓN PRUEBA TÉCNICA DESARROLLO FULLSTACK #

Prueba técnica Andrés Alexander Andrade Sánchez

### Objetivo###
- Mantenibilidad del código y buenas prácticas en gestión de desarrollo colaborativo
	- Creación de repositorio con tres ramas destinadas a: Parche, Feature 1 y Feature 2.
- Arquitectura:
	- Diagrama o plan para migración y/o despliegue en la nube. 
- Habilidades de Desarrollo:
	 - A partir del Modelo presentado:
	 	- API Restful con CRUD, base de Datos a elección.
		- Contenerizar la base de datos para aprovisionamiento.
		- Redactar instrucciones para despliegue local de la aplicación.
		
	- Implementar a través del Framework Angular:
		- Formulario para crear una 'Persona' permitiendo crear, modificar o borrar.
		- Validación de la base de datos.
		- Lista de todas las personas.
		
- Habilidades de Automatización:
	- Utilizar CircleCI para realizar pruebas unitarias.

### Instrucciones Despliegue API REST ###
- Para realizar el despliegue del API REST es necesario ingresar a la carpeta API y en ella abrir la consola de comandos:
	- Ejecutar el comando ```
		npm install
	```
	- Una vez ejecutado, el proyecto se encuentra configurado con Nodemon para la actualización constante del servidor. El cual se encuentra configurado en el puerto local 3000. Se ejecuta a través del comando ```
		npm run dev
	```
	- Cuando el servidor se encuentre en ejecución, dirigirse a la URL http://localhost:3000/ (En caso de encontrarse ocupado, modificar el archivo Index.js - Linea 12) y sobre ella ejecutar los métodos descritos a continuación:
		- Get: getPerson
		- Post: createPerson
		- Get: consultPerson/id
		- Delete: deletePerson/id
		- put: updatePerson/id
- La base de datos se encuentra en la Carpeta BASE DE DATOS, como un archivo Backup. Se abre directamente desde PosgreSQL. En caso de modificar los puertos o información adicional, se debe modificar el archivo del API Controllers/index.controller.js, en la instancia del Pool de Express (Linea 4).
	
