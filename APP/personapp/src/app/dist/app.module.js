"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_component_1 = require("./app.component");
var crear_persona_component_1 = require("./componentes/crear/crear-persona/crear-persona.component");
var ver_persona_component_1 = require("./componentes/ver/ver-persona/ver-persona.component");
var eliminar_persona_component_1 = require("./componentes/eliminar/eliminar-persona/eliminar-persona.component");
var modificar_persona_component_1 = require("./componentes/modificar/modificar-persona/modificar-persona.component");
var adoptar_persona_component_1 = require("./componentes/adoptar/adoptar-persona/adoptar-persona.component");
var animations_1 = require("@angular/platform-browser/animations");
var input_1 = require("@angular/material/input");
var datepicker_1 = require("@angular/material/datepicker");
var button_1 = require("@angular/material/button");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/common/http");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                crear_persona_component_1.CrearPersonaComponent,
                ver_persona_component_1.VerPersonaComponent,
                eliminar_persona_component_1.EliminarPersonaComponent,
                modificar_persona_component_1.ModificarPersonaComponent,
                adoptar_persona_component_1.AdoptarPersonaComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                input_1.MatInputModule,
                datepicker_1.MatDatepickerModule,
                button_1.MatButtonModule,
                forms_1.FormsModule,
                http_1.HttpClientModule
            ],
            providers: [],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
