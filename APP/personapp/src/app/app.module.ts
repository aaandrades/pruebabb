import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CrearPersonaComponent } from './componentes/crear/crear-persona/crear-persona.component';
import { VerPersonaComponent } from './componentes/ver/ver-persona/ver-persona.component';
import { EliminarPersonaComponent } from './componentes/eliminar/eliminar-persona/eliminar-persona.component';
import { ModificarPersonaComponent } from './componentes/modificar/modificar-persona/modificar-persona.component';
import { AdoptarPersonaComponent } from './componentes/adoptar/adoptar-persona/adoptar-persona.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    CrearPersonaComponent,
    VerPersonaComponent,
    EliminarPersonaComponent,
    ModificarPersonaComponent,
    AdoptarPersonaComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatDatepickerModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
