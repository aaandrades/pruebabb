//Instance Pool to connect PostgreSQL
const { Pool } = require('pg');
const { request } = require('express');
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    password: '1234',
    database: 'adoptar',
    port: '5432'
});

const createPerson = async(req, res) => {
    const { id, name, birth, email } = req.body;
    // console.log(`El id creado es: ${id}`);
    // CRUD / CREATE
    const response = await pool.query('INSERT INTO person (id,name,birth,email) VALUES ($1,$2,$3,$4)', [id, name, birth, email]);
    console.log(response);
    res.json({
        message: 'Persona agregada',
        body: {
            person: { id, name, birth, email }
        }
    })
    res.send('Persona agregada!');
}

// Async function to request the Database
const getPerson = async(req, res) => {
    // CRUD / READ
    const response = await pool.query('SELECT * FROM person');
    //Parce the Json
    res.status(200).json(response.rows);
    // console.log(response.rows);
    // res.send('Consulta de total de Personas');
}

const updatePerson = async(req, res) => {
    const id = req.params.id;
    const { name, birth, email } = req.body;
    console.log(name, birth, email);
    // CRUD / UPDATE
    const response = await pool.query('UPDATE person SET name = $1, birth =$2, email=$3 WHERE id=$4', [name, birth, email, id]);
    console.log(response);
    res.send('Persona actualizada correctamente');
}

const queryPerson = async(req, res) => {
    // res.send('El ID ingresado es: ' + req.params.id);
    // CRUD / READ
    const response = await pool.query('SELECT * FROM person WHERE ID = $1', [req.params.id]);
    console.log(response);
    res.json(response.rows);
}

const deletePerson = async(req, res) => {
    // res.send('Persona eliminada');
    // CRUD / DELETE
    const response = await pool.query('DELETE FROM person WHERE id = $1', [req.params.id]);
    console.log(response);
    res.json(`Persona eliminada con ID ${req.params.id} satisfactoriamente`);
}


//Export Functions
module.exports = {
    getPerson,
    createPerson,
    queryPerson,
    deletePerson,
    updatePerson
}