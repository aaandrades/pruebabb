import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-crear-persona',
  templateUrl: './crear-persona.component.html',
  styleUrls: ['./crear-persona.component.css']
})
export class CrearPersonaComponent implements OnInit {
  id = '';
  nombre = '';
  fecha = '';
  email: string;

  readonly ROOT_URL='http://localhost:3000/'

  //Obtener Valores del form
  getTextBoxVal(id,nombre,fecha,email) {
    console.log(id.value);
    console.log(nombre.value);
    console.log(fecha.value);
    console.log(email.value);
  }
  constructor() {
   }

  ngOnInit(): void {
  }

}
