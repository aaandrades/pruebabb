import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptarPersonaComponent } from './adoptar-persona.component';

describe('AdoptarPersonaComponent', () => {
  let component: AdoptarPersonaComponent;
  let fixture: ComponentFixture<AdoptarPersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptarPersonaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptarPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
